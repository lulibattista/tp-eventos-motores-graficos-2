using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class GameManager : MonoBehaviour
{
    public UnityEvent<string, string> EventoResultadoComparar;
    private Dictionary<string, Dictionary<string, string>> ListaResultado;
    public TMP_Text Resultado;


    
    void Start()
    {
        ControlComputadora Resultados = FindObjectOfType<ControlComputadora>();
        Resultados.EventoResultado += Ganador;
        ListaResultado = new Dictionary<string, Dictionary<string, string>>();


        ListaResultado["Piedra"] = new Dictionary<string, string>
        {
            { "Piedra", "Empataron" },
            { "Papel", "Gana PC" },
            { "Tijera", "Gana Jugador" }
        };

        ListaResultado["Papel"] = new Dictionary<string, string>
        {
            { "Piedra", "Gana Jugador" },
            { "Papel", "Empataron" },
            { "Tijera", "Gana PC" }
        };

        ListaResultado["Tijera"] = new Dictionary<string, string>
        {
            { "Piedra", "Gana PC" },
            { "Papel", "Gana Jugador" },
            { "Tijera", "Empataron" }
        };
    }

    
    void Update()
    {
        
    }
    public void Ganador()
    {
        
        string resultado = ListaResultado[EleccionJugador.EleccionFinal][ControlComputadora.EleccionCompuFinal];  
        Resultado.text = resultado;

    }
}
