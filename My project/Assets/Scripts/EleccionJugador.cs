using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class EleccionJugador : MonoBehaviour
{
    public TMP_Text BotonElejidoText;
    public Button BotonPiedra;
    public Button BotonPapel;
    public Button BotonTijera;
    public ControlComputadora controlComputadora;
    public delegate void EventoJuegoDelegate();
    public event EventoJuegoDelegate EventoJuego;
    public static string EleccionFinal;
    

    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
    public void Piedra()
    {
        
        BotonElejidoText.text = "Piedra";
        BotonPiedra.gameObject.SetActive(false);
        EleccionFinal = "Piedra";
        EventoJuego();
        
    }
    public void Papel()
    {
        BotonElejidoText.text = "Papel";
        BotonPapel.gameObject.SetActive(false);
        EleccionFinal = "Papel";
        EventoJuego();
    }
    public void Tijera()
    {
        BotonElejidoText.text = "Tijera";
        BotonTijera.gameObject.SetActive(false);
        EleccionFinal = "Tijera";
        EventoJuego();
    }
    public void Reset()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
