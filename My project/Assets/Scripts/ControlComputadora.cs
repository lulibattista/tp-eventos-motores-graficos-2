using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class ControlComputadora : MonoBehaviour
{
    private List<string> choices = new List<string>() { "Piedra", "Papel", "Tijera" };
    public TMP_Text ElejidoText;
    public delegate void EventoResultadoDelegate();
    public event EventoResultadoDelegate EventoResultado;
    public static string EleccionCompuFinal;



    void Start()
    {
        EleccionJugador JugoElJugador = FindObjectOfType<EleccionJugador>();
        JugoElJugador.EventoJuego += CompuChoice;
     
    }

    
    void Update()
    {
        
    }


    public string GetRandomChoice()
    {
        int randomIndex = Random.Range(0, choices.Count);
        return choices[randomIndex];
    }

    public void CompuChoice()
    {
        
        string randomChoice = GetRandomChoice();
        ElejidoText.text = randomChoice;
        EleccionCompuFinal = randomChoice;
        EventoResultado();
    }
}
